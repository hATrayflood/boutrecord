import os
import imp
import flask

from boutrecord import application
from boutrecord import db

from boutrecord.model import Player
from boutrecord.model import Channel
from boutrecord.model import Record
from boutrecord.model import RecordVersus
from boutrecord.model import Bout
from boutrecord.model import BoutResult

@application.route('/twitter-bootstrap/<path:dirname>/<path:filename>')
def twitter_bootstrap(dirname, filename):
	return flask.send_file('/'.join([application.config['TWITTER_BOOTSTRAP_FILES'], dirname, filename]))

@application.before_request
def before_request():
	flask.g.l10n = get_l10n()
	flask.g.db_session = db.create_scoped_session()

@application.teardown_request
def teardown_request(exc):
	flask.g.db_session.close()

@application.route('/')
def page_0_screen_name():
	subdomain = get_subdomain()
	if subdomain:
		return page_0_subdomain(subdomain)
	return index()

def page_0_subdomain(subdomain):
	channel = Channel.get_by_screen_name(flask.g.db_session, subdomain.replace('-', '_'))
	if channel:
		return page_channel(channel)
	flask.abort(404)

@application.route('/<string:screen_name1>')
def page_1_screen_name(screen_name1):
	subdomain = get_subdomain()
	if subdomain:
		return page_1_subdomain(subdomain, screen_name1)

	channel = Channel.get_by_screen_name(flask.g.db_session, screen_name1)
	player  = Player.get_by_screen_name(flask.g.db_session, screen_name1)
	if channel:
		return page_channel(channel)
	elif player:
		return page_player(player)
	flask.abort(404)

def page_1_subdomain(subdomain, screen_name1):
	channel = Channel.get_by_screen_name(flask.g.db_session, subdomain.replace('-', '_'))
	player  = Player.get_by_screen_name(flask.g.db_session, screen_name1)
	if channel and player:
		return page_channel_player(channel, player)
	flask.abort(404)

@application.route('/<string:screen_name1>/<string:screen_name2>')
def page_2_screen_name(screen_name1, screen_name2):
	subdomain = get_subdomain()
	if subdomain:
		return page_2_subdomain(subdomain, screen_name1, screen_name2)

	channel = Channel.get_by_screen_name(flask.g.db_session, screen_name1)
	if channel:
		player = Player.get_by_screen_name(flask.g.db_session, screen_name2)
		if player:
			return page_channel_player(channel, player)
	else:
		player   = Player.get_by_screen_name(flask.g.db_session, screen_name1)
		opponent = Player.get_by_screen_name(flask.g.db_session, screen_name2)
		if player and opponent:
			return page_player_opponent(player, opponent)
	flask.abort(404)

def page_2_subdomain(subdomain, screen_name1, screen_name2):
	channel  = Channel.get_by_screen_name(flask.g.db_session, subdomain.replace('-', '_'))
	player   = Player.get_by_screen_name(flask.g.db_session, screen_name1)
	opponent = Player.get_by_screen_name(flask.g.db_session, screen_name2)
	if channel and player and opponent:
		return page_channel_player_opponent(channel, player, opponent)
	flask.abort(404)

@application.route('/<string:screen_name1>/<string:screen_name2>/<string:screen_name3>')
def page_3_screen_name(screen_name1, screen_name2, screen_name3):
	subdomain = get_subdomain()
	if subdomain:
		flask.abort(404)

	channel  = Channel.get_by_screen_name(flask.g.db_session, screen_name1)
	player   = Player.get_by_screen_name(flask.g.db_session, screen_name2)
	opponent = Player.get_by_screen_name(flask.g.db_session, screen_name3)
	if channel and player and opponent:
		return page_channel_player_opponent(channel, player, opponent)
	flask.abort(404)


def get_subdomain():
	domain = application.config.get('SUBDOMAIN_ROOT', '')
	if not domain:
		return

	host = flask.request.host
	if host == domain:
		return
	if not host.endswith(domain):
		return

	return host[0:len(host) - len(domain) - 1]

def get_l10n():
	lang = flask.request.headers.get('Accept-Language', '')
	lang = lang.split(',')[0].split('-')[0]
	dir = os.path.dirname(os.path.abspath(__file__)) + '/l10n/%s.py'
	try:
		return imp.load_source('', dir % lang)
	except:
		return imp.load_source('', dir % 'en')

def page_channel_player_opponent(channel, player, opponent):
	context = {'l10n': flask.g.l10n}
	context['subdomain_root'] = application.config.get('SUBDOMAIN_ROOT', '')
	context['root']     = Channel.get_root(flask.g.db_session)
	context['design']   = player
	context['channel']  = channel
	context['player']   = player
	context['opponent'] = opponent
	context['records']     = [RecordVersus.get_by_player_opponent_channel(flask.g.db_session, player.id, opponent.id, channel.id)]
	context['best_bout']   = BoutResult.get_top_rt_by_opponent(flask.g.db_session, 50, player.id, opponent.id, channel.id)
	context['recent_bout'] = BoutResult.get_recent_by_opponent(flask.g.db_session, 50, player.id, opponent.id, channel.id)
	return flask.render_template('channel_player_opponent.html', **context)

def page_player_opponent(player, opponent):
	context = {'l10n': flask.g.l10n}
	context['subdomain_root'] = application.config.get('SUBDOMAIN_ROOT', '')
	context['root']     = Channel.get_root(flask.g.db_session)
	context['design']   = player
	context['player']   = player
	context['opponent'] = opponent
	context['records']     = RecordVersus.get_by_player_opponent(flask.g.db_session, player.id, opponent.id)
	context['best_bout']   = BoutResult.get_top_rt_by_opponent(flask.g.db_session, 50, player.id, opponent.id)
	context['recent_bout'] = BoutResult.get_recent_by_opponent(flask.g.db_session, 50, player.id, opponent.id)
	return flask.render_template('player_opponent.html', **context)

def page_channel_player(channel, player):
	context = {'l10n': flask.g.l10n}
	context['subdomain_root'] = application.config.get('SUBDOMAIN_ROOT', '')
	context['root']    = Channel.get_root(flask.g.db_session)
	context['design']  = player
	context['channel'] = channel
	context['player']  = player
	context['vs_record']   = Record.get_by_player_channel(flask.g.db_session, player.id, channel.id)
	context['superior']    = RecordVersus.get_top_win(flask.g.db_session, 20, player.id, channel.id)
	context['inferior']    = RecordVersus.get_top_lose(flask.g.db_session, 20, player.id, channel.id)
	context['best_bout']   = BoutResult.get_top_rt(flask.g.db_session, 20, player.id, channel.id)
	context['recent_bout'] = BoutResult.get_recent(flask.g.db_session, 20, player.id, channel.id)
	return flask.render_template('channel_player.html', **context)

def page_player(player):
	context = {'l10n': flask.g.l10n}
	context['subdomain_root'] = application.config.get('SUBDOMAIN_ROOT', '')
	context['root']   = Channel.get_root(flask.g.db_session)
	context['design'] = player
	context['player'] = player
	context['channels']    = Record.get_by_player(flask.g.db_session, player.id)
	context['superior']    = RecordVersus.get_top_win(flask.g.db_session, 20, player.id)
	context['inferior']    = RecordVersus.get_top_lose(flask.g.db_session, 20, player.id)
	context['best_bout']   = BoutResult.get_top_rt(flask.g.db_session, 20, player.id)
	context['recent_bout'] = BoutResult.get_recent(flask.g.db_session, 20, player.id)
	return flask.render_template('player.html', **context)

def page_channel(channel):
	context = {'l10n': flask.g.l10n}
	context['subdomain_root'] = application.config.get('SUBDOMAIN_ROOT', '')
	context['root']    = Channel.get_root(flask.g.db_session)
	context['design']  = channel
	context['channel'] = channel
	context['top_player']  = Record.get_top_win(flask.g.db_session, 20, channel.id)
	context['most_famous'] = Record.get_top_rt(flask.g.db_session, 20, channel.id)
	context['best_bout']   = Bout.get_top_rt(flask.g.db_session, 20, channel.id)
	context['recent_bout'] = Bout.get_recent(flask.g.db_session, 20, channel.id)
	return flask.render_template('channel.html', **context)

def index():
	context = {'l10n': flask.g.l10n}
	context['subdomain_root'] = application.config.get('SUBDOMAIN_ROOT', '')
	context['root'] = Channel.get_root(flask.g.db_session)
	context['channels']    = Channel.all(flask.g.db_session)
	context['top_player']  = Player.get_top_win(flask.g.db_session, 20)
	context['most_famous'] = Player.get_top_rt(flask.g.db_session, 20)
	context['best_bout']   = Bout.get_top_rt(flask.g.db_session, 20)
	context['recent_bout'] = Bout.get_recent(flask.g.db_session, 20)
	return flask.render_template('index.html', **context)
