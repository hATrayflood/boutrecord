import urllib2
from boutrecord import application
from boutrecord import db
from boutrecord import get_last
from sqlalchemy.schema import Index
from sqlalchemy.sql import func

class Player(db.Model):
	id = db.Column(db.BigInteger, primary_key=True, autoincrement=False)
	access_token        = db.Column(db.String(length=255))
	access_token_secret = db.Column(db.String(length=255))

	screen_name = db.Column(db.String(length=191, collation='utf8mb4_bin'))
	lang        = db.Column(db.String(length=255))
	time_zone   = db.Column(db.String(length=255))
	utc_offset  = db.Column(db.Integer)
	name        = db.Column(db.String(length=255))
	location    = db.Column(db.String(length=255))
	url         = db.Column(db.String(length=1022))
	description = db.Column(db.String(length=255))

	default_profile                    = db.Column(db.Boolean)
	default_profile_image              = db.Column(db.Boolean)
	profile_background_color           = db.Column(db.String(length=255))
	profile_background_image_url       = db.Column(db.String(length=1022))
	profile_background_image_url_https = db.Column(db.String(length=1022))
	profile_background_tile            = db.Column(db.Boolean)
	profile_image_url                  = db.Column(db.String(length=1022))
	profile_image_url_https            = db.Column(db.String(length=1022))
	profile_text_color                 = db.Column(db.String(length=255))
	profile_link_color                 = db.Column(db.String(length=255))
	profile_sidebar_border_color       = db.Column(db.String(length=255))
	profile_sidebar_fill_color         = db.Column(db.String(length=255))
	profile_use_background_image       = db.Column(db.Boolean)

	win  = db.Column(db.Integer)
	lose = db.Column(db.Integer)
	draw = db.Column(db.Integer)
	favorite_count = db.Column(db.Integer)
	retweet_count  = db.Column(db.Integer)
	recent_channel = db.Column(db.BigInteger)

	def __init__(self, id):
		self.id = id
		self.win  = 0
		self.lose = 0
		self.draw = 0
		self.favorite_count = 0
		self.retweet_count  = 0
		self.recent_channel = 0

	def set_from_twitter(self, user):
		self.screen_name = user.screen_name
		self.lang        = user.lang
		self.time_zone   = user.time_zone
		self.utc_offset  = user.utc_offset
		self.name        = user.name
		self.location    = user.location
		self.url         = user.url
		self.description = user.description

		if self.time_zone   == None:
			self.time_zone   = ''
		if self.utc_offset  == None:
			self.utc_offset  = 9 * 60 * 60
		if self.location    == None:
			self.location    = ''
		if self.url         == None:
			self.url         = ''
		if self.description == None:
			self.description = ''

		if self.url:
			try:
				self.url = urllib2.urlopen(self.url).url
			except:
				pass

		self.default_profile                    = user.default_profile
		self.default_profile_image              = user.default_profile_image
		self.profile_background_color           = user.profile_background_color
		self.profile_background_image_url       = user.profile_background_image_url
		self.profile_background_image_url_https = user.profile_background_image_url_https
		self.profile_background_tile            = user.profile_background_tile
		self.profile_image_url                  = user.profile_image_url
		self.profile_image_url_https            = user.profile_image_url_https
		self.profile_text_color                 = user.profile_text_color
		self.profile_link_color                 = user.profile_link_color
		self.profile_sidebar_border_color       = user.profile_sidebar_border_color
		self.profile_sidebar_fill_color         = user.profile_sidebar_fill_color
		self.profile_use_background_image       = user.profile_use_background_image

	def get_player(self):
		return self


	@classmethod
	def get_top_rt(cls, db_session, n): # index
		q = db_session.query(cls).filter(cls.retweet_count>0)
		return get_last(q, n, pkey=cls.id, order=cls.retweet_count)

	@classmethod
	def get_top_win(cls, db_session, n): # index
		q = db_session.query(cls).filter(cls.win>0)
		return get_last(q, n, pkey=cls.id, order=cls.win)

	@classmethod
	def get_by_id(cls, db_session, id): # total
		return db_session.query(cls).get(id)

	@classmethod
	def get_by_screen_name(cls, db_session, screen_name): # view
		return db_session.query(cls).filter_by(screen_name=screen_name).first()

	@classmethod
	def count_by_recent_channel(cls, db_session, recent_channel): # total
		return db_session.query(func.count(cls.id)).filter_by(recent_channel=recent_channel).first()[0]

Index('ix_player_screen_name', Player.screen_name)
Index('ix_player_retweet_count', Player.retweet_count)
Index('ix_player_win', Player.win)

class Channel(db.Model):
	id = db.Column(db.BigInteger, primary_key=True, autoincrement=False)
	access_token        = db.Column(db.String(length=255))
	access_token_secret = db.Column(db.String(length=255))
	root                = db.Column(db.Boolean)
	default_result      = db.Column(db.String(length=15))

	screen_name = db.Column(db.String(length=191, collation='utf8mb4_bin'))
	lang        = db.Column(db.String(length=255))
	time_zone   = db.Column(db.String(length=255))
	utc_offset  = db.Column(db.Integer)
	name        = db.Column(db.String(length=255))
	location    = db.Column(db.String(length=255))
	url         = db.Column(db.String(length=1022))
	description = db.Column(db.String(length=255))

	default_profile                    = db.Column(db.Boolean)
	default_profile_image              = db.Column(db.Boolean)
	profile_background_color           = db.Column(db.String(length=255))
	profile_background_image_url       = db.Column(db.String(length=1022))
	profile_background_image_url_https = db.Column(db.String(length=1022))
	profile_background_tile            = db.Column(db.Boolean)
	profile_image_url                  = db.Column(db.String(length=1022))
	profile_image_url_https            = db.Column(db.String(length=1022))
	profile_text_color                 = db.Column(db.String(length=255))
	profile_link_color                 = db.Column(db.String(length=255))
	profile_sidebar_border_color       = db.Column(db.String(length=255))
	profile_sidebar_fill_color         = db.Column(db.String(length=255))
	profile_use_background_image       = db.Column(db.Boolean)

	title    = db.Column(db.String(length=255))
	category = db.Column(db.String(length=255))
	bout     = db.Column(db.Integer)
	player   = db.Column(db.Integer)
	active_player  = db.Column(db.Integer)
	favorite_count = db.Column(db.Integer)
	retweet_count  = db.Column(db.Integer)

	label_win  = 'win'
	label_lose = 'lose'
	label_draw = 'draw'

	def __init__(self, id):
		self.id = id
		self.root = False
		self.default_result = Channel.label_lose
		self.bout   = 0
		self.player = 0
		self.active_player  = 0
		self.favorite_count = 0
		self.retweet_count  = 0

	def set_from_twitter(self, user):
		self.screen_name = user.screen_name
		self.lang        = user.lang
		self.time_zone   = user.time_zone
		self.utc_offset  = user.utc_offset
		self.name        = user.name
		self.location    = user.location
		self.url         = user.url
		self.description = user.description

		if self.time_zone   == None:
			self.time_zone   = ''
		if self.utc_offset  == None:
			self.utc_offset  = 9 * 60 * 60
		if self.location    == None:
			self.location    = ''
		if self.url         == None:
			self.url         = ''
		if self.description == None:
			self.description = ''

		if self.url:
			try:
				self.url = urllib2.urlopen(self.url).url
			except:
				pass

		self.default_profile                    = user.default_profile
		self.default_profile_image              = user.default_profile_image
		self.profile_background_color           = user.profile_background_color
		self.profile_background_image_url       = user.profile_background_image_url
		self.profile_background_image_url_https = user.profile_background_image_url_https
		self.profile_background_tile            = user.profile_background_tile
		self.profile_image_url                  = user.profile_image_url
		self.profile_image_url_https            = user.profile_image_url_https
		self.profile_text_color                 = user.profile_text_color
		self.profile_link_color                 = user.profile_link_color
		self.profile_sidebar_border_color       = user.profile_sidebar_border_color
		self.profile_sidebar_fill_color         = user.profile_sidebar_fill_color
		self.profile_use_background_image       = user.profile_use_background_image

	def get_channel(self):
		return self

	def get_base_url(self, request_host):
		subdomain_root = application.config.get('SUBDOMAIN_ROOT', '')
		if subdomain_root:
			return self.screen_name.replace('_', '-') + '.' + subdomain_root
		else:
			return request_host + '/' + self.screen_name


	@classmethod
	def get_root(cls, db_session): # tweetbot
		return db_session.query(cls).filter_by(root=True).first()

	@classmethod
	def all(cls, db_session): # index
		return db_session.query(cls).filter_by(root=False).order_by(cls.screen_name)

	@classmethod
	def get_by_id(cls, db_session, id): # total
		return db_session.query(cls).filter_by(root=False, id=id).first()

	@classmethod
	def get_by_screen_name(cls, db_session, screen_name): # view
		return db_session.query(cls).filter_by(root=False, screen_name=screen_name).first()

	@classmethod
	def get_sum(cls, db_session): # total
		return db_session.query(func.sum(cls.bout), func.sum(cls.active_player), func.sum(cls.favorite_count), func.sum(cls.retweet_count)).filter_by(root=False).first()

Index('ix_channel_root_screen_name', Channel.root, Channel.screen_name)
