import time
import datetime
import signal
import threading
import json
import operator
import logging

import tweepy
from tweepy.models import Status

from boutrecord import application
from boutrecord import db
from boutrecord import get_logger_handler

from boutrecord.model import Bout
from boutrecord.model import BoutResult
from boutrecord.model import Record
from boutrecord.model import RecordVersus
from boutrecord.model import Player
from boutrecord.model import Channel

def new_channel(db_session):
	if not Channel.get_root(db_session):
		init_root(db_session)

	print('prepare for new channel')
	auth = new_auth()
	api  = tweepy.API(auth)
	user = api.me()

	title    = raw_input("title: ")
	category = raw_input("category: ")

	channel = Channel.get_by_id(db_session, user.id)
	if not channel:
		channel = Channel(user.id)
		db_session.add(channel)
	channel.set_from_twitter(user)

	channel.title    = title
	channel.category = category
	channel.access_token        = auth.access_token.key
	channel.access_token_secret = auth.access_token.secret
	db_session.commit()

def init_root(db_session):
	print('prepare for root')
	auth = new_auth()
	api  = tweepy.API(auth)
	user = api.me()

	root = Channel(user.id)
	root.set_from_twitter(user)
	root.root = True
	root.access_token        = auth.access_token.key
	root.access_token_secret = auth.access_token.secret
	db_session.add(root)
	db_session.commit()

def new_auth():
	consumer_key    = application.config['CONSUMER_KEY']
	consumer_secret = application.config['CONSUMER_SECRET']
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret, secure=True)
	print(auth.get_authorization_url())
	verifier = raw_input("verifier: ")
	auth.get_access_token(verifier)
	return auth


def new_bout(db_session, tweet, opponent):
	(place, finish, result) = read_tweet(tweet, opponent)

	channel = tweet.channel
	user    = tweet.author
	c = Channel.get_by_id(db_session, channel)
	if not result:
		result  = c.default_result
	if result == Channel.label_draw:
		result1 = Channel.label_draw
	elif result == Channel.label_lose:
		result1 = Channel.label_win
	else:
		result1 = Channel.label_lose

	b = Bout(user.id, tweet.id, tweet.created_at, user.utc_offset, opponent.id, channel, result, place, finish)
	b.favorite_count = tweet.favorite_count
	b.retweet_count  = tweet.retweet_count
	db_session.add(b)
	db_session.flush()

	b1 = BoutResult(b.id, opponent.id, user.id, channel, result1)
	b2 = BoutResult(b.id, user.id, opponent.id, channel, result)
	b1.favorite_count = tweet.favorite_count
	b2.favorite_count = tweet.favorite_count
	b1.retweet_count  = tweet.retweet_count
	b2.retweet_count  = tweet.retweet_count
	db_session.add(b1)
	db_session.add(b2)

	db_session.commit()
	total(db_session, channel, user, opponent)

def read_tweet(tweet, opponent):
	tweet_text = tweet.text
	place = ''

	for user_mention in tweet.entities['user_mentions']:
		indices = user_mention['indices']
		if user_mention['id'] == tweet.channel:
			tweet_text = fill_space(tweet_text, indices[0], indices[1])
		if user_mention['id'] == opponent.id:
			tweet_text = fill_space(tweet_text, indices[0], indices[1])

	if tweet.entities['hashtags']:
		hashtag = tweet.entities['hashtags'][0]
		place   = hashtag['text']
		indices = hashtag['indices']
		tweet_text = fill_space(tweet_text, indices[0], indices[1])

	result = None
	words = tweet_text.split()
	for word in words:
		if word == Channel.label_draw:
			result = Channel.label_draw
			break
		if word == Channel.label_lose:
			result = Channel.label_lose
			break
		if word == Channel.label_win:
			result = Channel.label_win
			break
	finish = ' '.join(words)

	return (place, finish, result)

def fill_space(tweet_text, start, end):
	spaces = ''
	for i in range(end - start):
		spaces += ' '
	return tweet_text[0:start] + spaces + tweet_text[end:]

def update_favorite_retweet(db_session, tweet, opponent):
	user = tweet.author
	b = Bout.get_by_status(db_session, user.id, tweet.id)
	if not b:
		return
	if b.favorite_count == tweet.favorite_count and b.retweet_count == tweet.retweet_count:
		return

	b.favorite_count = tweet.favorite_count
	b.retweet_count  = tweet.retweet_count
	channel = b.channel

	for br in BoutResult.get_by_bout(db_session, b.id):
		br.favorite_count = tweet.favorite_count
		br.retweet_count  = tweet.retweet_count

	db_session.commit()
	total(db_session, channel, user, opponent)

def delete_bout(db_session, tweet, user, opponent):
	b = Bout.get_by_status(db_session, user.id, tweet)
	if not b:
		return

	db_session.delete(b)
	channel = b.channel

	for br in BoutResult.get_by_bout(db_session, b.id):
		db_session.delete(br)

	db_session.commit()
	total(db_session, channel, user, opponent)

def total(db_session, channel, player, opponent):
	r = Channel.get_root(db_session)
	c = Channel.get_by_id(db_session, channel)

	p = Player.get_by_id(db_session, player.id)
	if not p:
		p = Player(player.id)
		db_session.add(p)
	p.set_from_twitter(player)
	o = Player.get_by_id(db_session, opponent.id)
	if not o:
		o = Player(opponent.id)
		db_session.add(o)
	o.set_from_twitter(opponent)

	pr_ = Record.get_by_player_channel(db_session, player.id, channel)
	if not pr_:
		pr_ = Record(player.id, channel)
		db_session.add(pr_)
	or_ = Record.get_by_player_channel(db_session, opponent.id, channel)
	if not or_:
		or_ = Record(opponent.id, channel)
		db_session.add(or_)

	prv = RecordVersus.get_by_player_opponent_channel(db_session, player.id, opponent.id, channel)
	if not prv:
		prv = RecordVersus(player.id, opponent.id, channel)
		db_session.add(prv)
	orv = RecordVersus.get_by_player_opponent_channel(db_session, opponent.id, player.id, channel)
	if not orv:
		orv = RecordVersus(opponent.id, player.id, channel)
		db_session.add(orv)

	prv.win  = BoutResult.count_win(db_session, player.id, opponent.id, channel)
	prv.lose = BoutResult.count_lose(db_session, player.id, opponent.id, channel)
	prv.draw = BoutResult.count_draw(db_session, player.id, opponent.id, channel)
	(prv.favorite_count, prv.retweet_count) = BoutResult.get_sum(db_session, player.id, opponent.id, channel)
	orv.win  = BoutResult.count_win(db_session, opponent.id, player.id, channel)
	orv.lose = BoutResult.count_lose(db_session, opponent.id, player.id, channel)
	orv.draw = BoutResult.count_draw(db_session, opponent.id, player.id, channel)
	(orv.favorite_count, orv.retweet_count) = BoutResult.get_sum(db_session, opponent.id, player.id, channel)
	db_session.flush()

	(pr_.win, pr_.lose, pr_.draw, pr_.favorite_count, pr_.retweet_count) = RecordVersus.get_sum(db_session, player.id, channel)
	(or_.win, or_.lose, or_.draw, or_.favorite_count, or_.retweet_count) = RecordVersus.get_sum(db_session, opponent.id, channel)
	db_session.flush()

	active_player_updates = [c]
	if p.recent_channel != channel:
		active_player_updates.append(Channel.get_by_id(db_session, p.recent_channel))
		p.recent_channel = channel
	if o.recent_channel != channel:
		active_player_updates.append(Channel.get_by_id(db_session, o.recent_channel))
		o.recent_channel = channel

	(p.win, p.lose, p.draw, p.favorite_count, p.retweet_count) = Record.get_sum(db_session, player.id)
	(o.win, o.lose, o.draw, o.favorite_count, o.retweet_count) = Record.get_sum(db_session, opponent.id)
	db_session.flush()

	c.bout   = Bout.count_by_channel(db_session, channel)
	c.player = Record.count_by_channel(db_session, channel)
	(c.favorite_count, c.retweet_count) = Bout.get_sum(db_session, channel)

	for update_channel in active_player_updates:
		if update_channel:
			update_channel.active_player = Player.count_by_recent_channel(db_session, update_channel.id)
	db_session.flush()

	(r.bout, r.player, r.favorite_count, r.retweet_count) = Channel.get_sum(db_session)
	db_session.commit()


error_log         = 'Error occurred in %s ...'
error_log_channel = 'Error occurred in %s, channel %s ...'

class TweetBotStreamListener(tweepy.StreamListener):
	def __init__(self, api, daemon, channel):
		tweepy.StreamListener.__init__(self, api)
		self.daemon = daemon
		self.channel_id          = channel.id
		self.channel_screen_name = channel.screen_name

	def on_data(self, data):
		try:
			json_data = json.loads(data)
			if 'disconnect' in json_data:
				json_data = json_data['disconnect']
				raise Exception('disconnect: code %s, stream_name %s, reason %s' % (json_data.get('code'), json_data.get('stream_name'), json_data.get(['reason'])))
			elif 'delete' in json_data:
				json_data = json_data['delete']['status']
				json_data['event'] = 'delete'
			elif 'retweeted_status' in json_data:
				json_data = json_data['retweeted_status']
				json_data['event'] = 'retweet'
			elif json_data.get('event') == 'favorite':
				json_data = json_data['target_object']
				json_data['event'] = 'favorite'
			else:
				json_data['event'] = ''
			json_data['channel'] = self.channel_id
			json_data['channel_screen_name'] = self.channel_screen_name

			if 'user' in json_data and 'id' in json_data and 'text' in json_data:
				self.daemon.add_tweet(Status.parse(self.api, json_data))
			elif json_data.get('event') == 'delete':
				status = Status.parse(self.api, json_data)
				user   = self.api.get_user(user_id=status.user_id)
				setattr(status, 'author', user)
				setattr(status, 'created_at', datetime.datetime.utcnow())
				self.daemon.add_tweet(status)
		except:
			self.daemon.application_logger.exception(error_log_channel + '\n' + data, self.__class__.__name__, self.channel_screen_name)

class TweetBotStreamThread(threading.Thread):
	def __init__(self, stream, daemon, channel):
		threading.Thread.__init__(self)
		self.stream = stream
		self.daemon = daemon
		self.channel_screen_name = channel.screen_name

	def run(self):
		try:
			self.stream.userstream()
		except:
			self.daemon.application_logger.exception(error_log_channel, self.__class__.__name__, self.channel_screen_name)

class TweetBot:
	def __init__(self, pidfile_path, logfile_path):
		self.stdin_path      = '/dev/null'
		try:
			with open('/dev/tty', 'w+'):
				pass
			self.stdout_path     = '/dev/tty'
			self.stderr_path     = '/dev/tty'
		except:
			self.stdout_path     = '/dev/null'
			self.stderr_path     = '/dev/null'
		self.pidfile_path    = pidfile_path
		self.pidfile_timeout = 60

		self.signal_map   = {signal.SIGTERM: self.terminate, signal.SIGHUP: self.reload}
		self.running      = False
		self.logfile_path = logfile_path

		self.channels    = {}
		self.channel_api = {}
		self.tweets      = []
		self.tweets_lock     = threading.Lock()
		self.db_session_lock = threading.Lock()

	def add_tweet(self, tweet):
		with self.tweets_lock:
			self.tweets.append(tweet)

	def pop_tweet(self):
		with self.tweets_lock:
			if self.tweets:
				self.tweets.sort(key=operator.attrgetter('created_at'))
				return self.tweets.pop(0)
			return None

	def dispatch_tweet(self, tweet):
		db_session = db.create_scoped_session()
		try:
			with self.db_session_lock:
				self._dispatch_tweet(db_session, tweet)
		except:
			self.application_logger.exception(error_log + '\n' + 'user %s, tweet %s', 'dispatch_tweet', tweet.author.screen_name, tweet.id)
		finally:
			db_session.close()

	def _dispatch_tweet(self, db_session, tweet):
		if tweet.event == 'delete':
			bout = Bout.get_by_status(db_session, tweet.author.id, tweet.id)
			if not bout:
				return
			opponent = self.api.get_user(user_id=bout.opponent)
			delete_bout(db_session, tweet.id, tweet.author, opponent)
		elif tweet.event == 'retweet' or tweet.event == 'favorite':
			bout = Bout.get_by_status(db_session, tweet.author.id, tweet.id)
			if not bout:
				return
			opponent = self.api.get_user(user_id=bout.opponent)
			update_favorite_retweet(db_session, tweet, opponent)
		elif not tweet.in_reply_to_status_id:
			opponent = None
			if tweet.author.protected:
				return
			if not tweet.entities['user_mentions']:
				return
			if tweet.entities['user_mentions'][0]['id'] != tweet.channel:
				return
			for user_mention in tweet.entities['user_mentions']:
				if user_mention['id'] == tweet.channel:
					continue
				if user_mention['id'] == tweet.author.id:
					continue
				opponent = self.api.get_user(user_id=user_mention['id'])
				break
			if not opponent:
				for word in tweet.text.split():
					if word[0] == '@':
						continue
					if word == Channel.label_draw:
						continue
					if word == Channel.label_lose:
						continue
					if word == Channel.label_win:
						continue
					if word == tweet.channel_screen_name:
						continue
					if word == tweet.author.screen_name:
						continue
					try:
						opponent = self.api.get_user(screen_name=word)
						break
					except:
						pass
			if not opponent:
				return

			bout = Bout.get_by_status(db_session, tweet.author.id, tweet.id)
			if not bout:
				new_bout(db_session, tweet, opponent)
			else:
				update_favorite_retweet(db_session, tweet, opponent)
		else:
			user_id  = tweet.author.id
			tweet_id = tweet.in_reply_to_status_id
			bout = Bout.get_by_status(db_session, user_id, tweet_id)
			if not bout:
				return
			if 'delete' in tweet.text.split():
				opponent = self.api.get_user(user_id=bout.opponent)
				delete_bout(db_session, tweet_id, tweet.author, opponent)

	def get_tweepy_api(self, channel):
		consumer_key    = application.config['CONSUMER_KEY']
		consumer_secret = application.config['CONSUMER_SECRET']
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret, secure=True)
		auth.set_access_token(channel.access_token, channel.access_token_secret)
		return tweepy.API(auth)

	def get_userstream(self, api, channel):
		stream = tweepy.Stream(api.auth, TweetBotStreamListener(api, self, channel))
		TweetBotStreamThread(stream, self, channel).start()
		return stream

	def start_userstream(self):
		db_session = db.create_scoped_session()
		try:
			with self.db_session_lock:
				for channel in Channel.all(db_session):
					self._start_userstream(channel)
				db_session.commit()
			self.logger.info('remaining %s tweets', len(self.tweets))
		except:
			self.application_logger.exception(error_log, 'start_userstream')
		finally:
			db_session.close()

	def _start_userstream(self, channel):
		try:
			api = self.get_tweepy_api(channel)
			channel.set_from_twitter(api.get_user(user_id=channel.id))
			for tweet in api.mentions_timeline(count=200):
				setattr(tweet, 'channel', channel.id)
				setattr(tweet, 'channel_screen_name', channel.screen_name)
				setattr(tweet, 'event', '')
				self.add_tweet(tweet)
			if not channel.id in self.channels:
				self.channels[channel.id] = self.get_userstream(api, channel)
			if not channel.id in self.channel_api:
				self.channel_api[channel.id] = api
		except:
			self.application_logger.exception(error_log + '\n' + 'channel %s', 'start_userstream', channel.screen_name)

	def stop_userstream(self):
		try:
			for (channel_id, stream) in self.channels.items():
				stream.disconnect()
		except:
			self.application_logger.exception(error_log, 'stop_userstream')

	def reload(self, signal_number, stack_frame):
		self.logger.info('reload ...')
		self.start_userstream()

	def terminate(self, signal_number, stack_frame):
		if not self.running:
			return
		self.logger.info('terminate ...')
		self.stop_userstream()
		self.running = False
		self.logger.info('remaining %s tweets', len(self.tweets))

	def startup(self):
		self.logger = logging.getLogger(self.__class__.__name__)
		self.logger.addHandler(logging.FileHandler(self.logfile_path))
		self.logger.setLevel(logging.INFO)
		if application.debug:
			self.logger.setLevel(logging.DEBUG)
		self.application_logger = logging.getLogger(self.__class__.__name__ + '_application')
		self.application_logger.addHandler(get_logger_handler())
		self.logger.info('startup ...')

		self.running = True
		db_session = db.create_scoped_session()
		try:
			root = Channel.get_root(db_session)
			self.api = self.get_tweepy_api(root)
			root.set_from_twitter(self.api.get_user(user_id=root.id))
			db_session.commit()
			self.start_userstream()
		except:
			self.application_logger.exception(error_log, 'startup')
		finally:
			db_session.close()

	def run_loop(self):
		try:
			tweet = self.pop_tweet()
			if tweet:
				self.dispatch_tweet(tweet)
			else:
				time.sleep(1)
			self.logger.debug('remaining %s tweets', len(self.tweets))
		except:
			self.application_logger.exception(error_log, 'run_loop')

	def run(self):
		self.startup()
		while self.running or self.tweets:
			self.run_loop()
		self.logger.info('stop ...')
