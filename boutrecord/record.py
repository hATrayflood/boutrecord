from boutrecord import db
from boutrecord import get_last
from sqlalchemy.schema import Index
from sqlalchemy.sql import func

from boutrecord.player import Player
from boutrecord.player import Channel

class Record(db.Model):
	id = db.Column(db.BigInteger, primary_key=True)
	player  = db.Column(db.BigInteger)
	channel = db.Column(db.BigInteger)

	win  = db.Column(db.Integer)
	lose = db.Column(db.Integer)
	draw = db.Column(db.Integer)
	favorite_count = db.Column(db.Integer)
	retweet_count  = db.Column(db.Integer)

	def __init__(self, player, channel):
		self.player  = player
		self.channel = channel
		self.win  = 0
		self.lose = 0
		self.draw = 0
		self.favorite_count = 0
		self.retweet_count  = 0

	def get_player(self):
		return Player.query.get(self.player)

	def get_channel(self):
		return Channel.query.get(self.channel)


	@classmethod
	def get_by_player(cls, db_session, player): # page_player
		return db_session.query(cls).filter_by(player=player)

	@classmethod
	def count_by_channel(cls, db_session, channel): # total
		return db_session.query(func.count(cls.id)).filter_by(channel=channel).first()[0]

	@classmethod
	def get_by_player_channel(cls, db_session, player, channel): # total
		return db_session.query(cls).filter_by(player=player, channel=channel).first()

	@classmethod
	def get_sum(cls, db_session, player): # total
		return db_session.query(func.sum(cls.win), func.sum(cls.lose), func.sum(cls.draw), func.sum(cls.favorite_count), func.sum(cls.retweet_count)).filter_by(player=player).first()


	@classmethod
	def get_top_rt(cls, db_session, n, channel): # page_channel
		q = db_session.query(cls).filter(cls.channel==channel, cls.retweet_count>0)
		return get_last(q, n, pkey=cls.id, order=cls.retweet_count)

	@classmethod
	def get_top_win(cls, db_session, n, channel): # page_channel
		q = db_session.query(cls).filter(cls.channel==channel, cls.win>0)
		return get_last(q, n, pkey=cls.id, order=cls.win)

Index('ix_record_player_channel', Record.player, Record.channel)
Index('ix_record_channel_retweet_count', Record.channel, Record.retweet_count)
Index('ix_record_channel_win', Record.channel, Record.win)

class RecordVersus(db.Model):
	id = db.Column(db.BigInteger, primary_key=True)
	player   = db.Column(db.BigInteger)
	opponent = db.Column(db.BigInteger)
	channel  = db.Column(db.BigInteger)

	win  = db.Column(db.Integer)
	lose = db.Column(db.Integer)
	draw = db.Column(db.Integer)
	favorite_count = db.Column(db.Integer)
	retweet_count  = db.Column(db.Integer)

	def __init__(self, player, opponent, channel):
		self.player   = player
		self.opponent = opponent
		self.channel  = channel
		self.win  = 0
		self.lose = 0
		self.draw = 0
		self.favorite_count = 0
		self.retweet_count  = 0

	def get_opponent(self):
		return Player.query.get(self.opponent)

	def get_channel(self):
		return Channel.query.get(self.channel)


	@classmethod
	def get_by_player_opponent(cls, db_session, player, opponent): # page_player_opponent
		return db_session.query(cls).filter_by(player=player, opponent=opponent)

	@classmethod
	def get_by_player_opponent_channel(cls, db_session, player, opponent, channel): # total
		return db_session.query(cls).filter_by(player=player, opponent=opponent, channel=channel).first()

	@classmethod
	def get_sum(cls, db_session, player, channel): # total
		return db_session.query(func.sum(cls.win), func.sum(cls.lose), func.sum(cls.draw), func.sum(cls.favorite_count), func.sum(cls.retweet_count)).filter_by(player=player, channel=channel).first()


	@classmethod
	def get_top_win(cls, db_session, n, player, channel=None): # page_player page_channel_player
		q = db_session.query(cls).filter(cls.player==player, cls.win>cls.lose)
		if channel:
			q = q.filter_by(channel=channel)
		return get_last(q, n, pkey=cls.id, order=cls.win)

	@classmethod
	def get_top_lose(cls, db_session, n, player, channel=None): # page_player page_channel_player
		q = db_session.query(cls).filter(cls.player==player, cls.lose>cls.win)
		if channel:
			q = q.filter_by(channel=channel)
		return get_last(q, n, pkey=cls.id, order=cls.lose)

Index('ix_record_versus_player_opponent_channel', RecordVersus.player, RecordVersus.opponent, RecordVersus.channel)
Index('ix_record_versus_player_win' , RecordVersus.player, RecordVersus.win)
Index('ix_record_versus_player_lose', RecordVersus.player, RecordVersus.lose)
