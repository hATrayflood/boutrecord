# -*- coding: utf-8 -*-

BOUT        = u'bout'
PLAYER      = u'player'
WIN         = u'win'
LOSE        = u'lose'
DRAW        = u'draw'
RT          = u'RT'
VS          = u'VS'

CHANNEL     = u'channel'
TOP_PLAYER  = u'top player'
MOST_FAMOUS = u'most famous'
SUPERIOR    = u'superior'
INFERIOR    = u'inferior'

BEST_BOUT   = u'best bout'
RECENT_BOUT = u'recent bout'
DATETIME    = u'datetime'
WINNER      = u'winner'
LOSER       = u'loser'
DRAW_GAME   = u'DRAW GAME'
PLACE       = u'place or event'
FINISH      = u'finish or comment'
