# -*- coding: utf-8 -*-

BOUT        = u'試合'
PLAYER      = u'プレイヤー'
WIN         = u'勝'
LOSE        = u'敗'
DRAW        = u'分け'
RT          = u'RT'
VS          = u'対'

CHANNEL     = u'競技種目'
TOP_PLAYER  = u'実力ランク'
MOST_FAMOUS = u'人気ランク'
SUPERIOR    = u'勝ち越し'
INFERIOR    = u'負け越し'

BEST_BOUT   = u'注目の試合'
RECENT_BOUT = u'最近の試合'
DATETIME    = u'日時'
WINNER      = u'勝ち'
LOSER       = u'負け'
DRAW_GAME   = u'引き分け'
PLACE       = u'場所or大会'
FINISH      = u'決まり手orコメント'
