import os
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func

application = Flask(__name__)
if 'HOME' in os.environ and os.path.exists(os.environ['HOME'] + '/.brconfig'):
	application.config.from_pyfile(os.environ['HOME'] + '/.brconfig')
else:
	application.config.from_pyfile('/etc/boutrecord.config')
application.config['SECRET_KEY'] = __name__
db = SQLAlchemy(application)

def get_logger_handler():
	import logging
	from logging.handlers import SMTPHandler
	host     = application.config['SMTP_HOST']
	port     = application.config['SMTP_PORT']
	toaddrs  = application.config['SMTP_TOADDRS']
	fromaddr = application.config['SMTP_FROMADDR']
	handler  = SMTPHandler((host, port), fromaddr, toaddrs, 'boutrecord traceback')
	return handler
application.logger.addHandler(get_logger_handler())

def get_last(query, number, page=1, count=None, pkey=None, order=None):
	if count == None:
		if pkey:
			count = query.with_entities(func.count(pkey)).first()[0]
		else:
			count = query.count()
	if order:
		query = query.order_by(order)
	if number:
		if count > number * page:
			query = query.offset(count - number * page).limit(number)
		else:
			query = query.limit(count % number)
	return reversed(query.all())
