import datetime
from boutrecord import db
from boutrecord import get_last
from sqlalchemy.schema import Index
from sqlalchemy.sql import func
from boutrecord.player import Player
from boutrecord.player import Channel

class Bout(db.Model):
	id    = db.Column(db.BigInteger, primary_key=True)
	user  = db.Column(db.BigInteger)
	tweet = db.Column(db.BigInteger)

	created_at     = db.Column(db.DateTime)
	utc_offset     = db.Column(db.Integer)
	favorite_count = db.Column(db.Integer)
	retweet_count  = db.Column(db.Integer)

	opponent = db.Column(db.BigInteger)
        channel  = db.Column(db.BigInteger)
	result   = db.Column(db.String(length=15))
	place    = db.Column(db.String(length=255))
	finish   = db.Column(db.String(length=255))

	def __init__(self, user, tweet, created_at, utc_offset, opponent, channel, result, place='', finish=''):
		self.user  = user
		self.tweet = tweet
		self.created_at = created_at
		self.utc_offset = utc_offset
		if self.utc_offset == None:
			self.utc_offset  = 9 * 60 * 60
		self.opponent = opponent
		self.channel  = channel
		self.result   = result
		self.place    = place
		self.finish   = finish
		self.favorite_count = 0
		self.retweet_count  = 0

	def get_bout(self):
		return self

	def strf_created_at(self, utc_offset=None):
		if not utc_offset:
			utc_offset = self.utc_offset
		return (self.created_at + datetime.timedelta(seconds=utc_offset)).strftime('%Y/%m/%d %H:%M')

	def get_channel(self):
		return Channel.query.get(self.channel)

	def get_user(self):
		return Player.query.get(self.user)

	def get_winner(self):
		if self.result == Channel.label_win:
			player = self.user
		if self.result == Channel.label_lose:
			player = self.opponent
		if self.result == Channel.label_draw:
			if self.get_channel().default_result == Channel.label_win:
				player = self.user
			else:
				player = self.opponent
		return Player.query.get(player)

	def get_loser(self):
		if self.result == Channel.label_win:
			player = self.opponent
		if self.result == Channel.label_lose:
			player = self.user
		if self.result == Channel.label_draw:
			if self.get_channel().default_result == Channel.label_lose:
				player = self.user
			else:
				player = self.opponent
		return Player.query.get(player)


	@classmethod
	def get_by_id(cls, db_session, id):
		return db_session.query(cls).get(id)

	@classmethod
	def get_by_status(cls, db_session, user, tweet): # _update_favorite_retweet _delete_bout
		return db_session.query(cls).filter_by(user=user, tweet=tweet).first()

	@classmethod
	def count_by_channel(cls, db_session, channel): # total
		return db_session.query(func.count(cls.id)).filter_by(channel=channel).first()[0]

	@classmethod
	def get_sum(cls, db_session, channel): # total
		return db_session.query(func.sum(cls.favorite_count), func.sum(cls.retweet_count)).filter_by(channel=channel).first()


	@classmethod
	def get_top_rt(cls, db_session, n, channel=None): # index page_channel
		q = db_session.query(cls).filter(cls.retweet_count>0)
		if channel:
			q = q.filter_by(channel=channel)
		return get_last(q, n, pkey=cls.id, order=cls.retweet_count)

	@classmethod
	def get_recent(cls, db_session, n, channel=None): # index page_channel
		q = db_session.query(cls)
		if channel:
			q = q.filter_by(channel=channel)
			count = Channel.get_by_id(db_session, channel).bout
		else:
			count = Channel.get_root(db_session).bout
		return get_last(q, n, count=count, order=cls.id)

Index('ix_bout_channel', Bout.channel)
Index('ix_bout_retweet_count', Bout.retweet_count)
Index('ix_bout_status', Bout.user, Bout.tweet)

class BoutResult(db.Model):
	id     = db.Column(db.BigInteger, primary_key=True)
	bout   = db.Column(db.BigInteger)
	player = db.Column(db.BigInteger)

	opponent = db.Column(db.BigInteger)
        channel  = db.Column(db.BigInteger)
	result   = db.Column(db.String(length=15))
	favorite_count = db.Column(db.Integer)
	retweet_count  = db.Column(db.Integer)

	def __init__(self, bout, player, opponent, channel, result):
		self.bout     = bout
		self.player   = player
		self.opponent = opponent
		self.channel  = channel
		self.result   = result
		self.favorite_count = 0
		self.retweet_count  = 0

	def get_bout(self):
		return Bout.query.get(self.bout)


	@classmethod
	def get_by_bout(cls, db_session, bout): # _update_favorite_retweet _delete_bout
		return db_session.query(cls).filter_by(bout=bout)

	@classmethod
	def _count_by_result(cls, db_session, player, opponent, channel, result): # total
		return db_session.query(func.count(cls.id)).filter_by(player=player, opponent=opponent, channel=channel, result=result).first()[0]

	@classmethod
	def count_win(cls, db_session, player, opponent, channel): # total
		return cls._count_by_result(db_session, player, opponent, channel, Channel.label_win)

	@classmethod
	def count_lose(cls, db_session, player, opponent, channel): # total
		return cls._count_by_result(db_session, player, opponent, channel, Channel.label_lose)

	@classmethod
	def count_draw(cls, db_session, player, opponent, channel): # total
		return cls._count_by_result(db_session, player, opponent, channel, Channel.label_draw)

	@classmethod
	def get_sum(cls, db_session, player, opponent, channel): # total
		return db_session.query(func.sum(cls.favorite_count), func.sum(cls.retweet_count)).filter_by(player=player, opponent=opponent, channel=channel).first()


	@classmethod
	def get_top_rt(cls, db_session, n, player, channel=None): # page_player page_channel_player
		q = db_session.query(cls).filter(cls.player==player, cls.retweet_count>0)
		if channel:
			q = q.filter_by(channel=channel)
		return get_last(q, n, pkey=cls.id, order=cls.retweet_count)

	@classmethod
	def get_recent(cls, db_session, n, player, channel=None): # page_player page_channel_player
		q = db_session.query(cls).filter(cls.player==player)
		if channel:
			q = q.filter_by(channel=channel)
		return get_last(q, n, pkey=cls.id, order=cls.id)

	@classmethod
	def get_top_rt_by_opponent(cls, db_session, n, player, opponent, channel=None): # page_player_opponent page_channel_player_opponent
		q = db_session.query(cls).filter(cls.player==player, cls.opponent==opponent, cls.retweet_count>0)
		if channel:
			q = q.filter_by(channel=channel)
		return get_last(q, n, pkey=cls.id, order=cls.retweet_count)

	@classmethod
	def get_recent_by_opponent(cls, db_session, n, player, opponent, channel=None): # page_player_opponent page_channel_player_opponent
		q = db_session.query(cls).filter(cls.player==player, cls.opponent==opponent)
		if channel:
			q = q.filter_by(channel=channel)
		return get_last(q, n, pkey=cls.id, order=cls.id)

Index('ix_bout_result_bout', BoutResult.bout)
Index('ix_bout_result_player_retweet_count', BoutResult.player, BoutResult.retweet_count)
