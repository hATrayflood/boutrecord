#!/usr/bin/env python

import sys
import os

dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(dir)
os.chdir(dir)

import signal
from daemon.runner import DaemonRunner

from boutrecord import application
from boutrecord.tweetbot import TweetBot

# DaemonRunner instance method
def reload(self):
	os.kill(self.pidfile.read_pid(), signal.SIGHUP)
DaemonRunner.action_funcs['reload'] = reload

if __name__ == '__main__':
	pidfile_path = application.config['TWEETBOT_PID']
	logfile_path = application.config['TWEETBOT_LOG']
	daemon = TweetBot(pidfile_path, logfile_path)
	daemon_runner = DaemonRunner(daemon)
	daemon_runner.daemon_context.signal_map = daemon.signal_map
	daemon_runner.do_action()
