#!/bin/bash

sudo mkdir -p /var/log/boutrecord
sudo chown www-data:www-data /var/log/boutrecord
sudo cp initscript /etc/init.d/boutrecord-tweetbot
sudo update-rc.d boutrecord-tweetbot defaults 99 01
#sudo update-rc.d -f boutrecord-tweetbot remove
