#!/usr/bin/env python

import sys
import os

dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(dir)
os.chdir(dir)

from boutrecord import application
import boutrecord.view

if __name__ == '__main__':
	application.run(host='0.0.0.0', debug=True)
