#!/usr/bin/env python

import sys
import os

dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(dir)
os.chdir(dir)

from boutrecord import db
from boutrecord import tweetbot
db_session = db.create_scoped_session()
tweetbot.new_channel(db_session)
